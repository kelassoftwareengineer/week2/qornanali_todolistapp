# todolistapp

A simple java application to manage daily task

## Prequisites

1. OpenJDK `8`. You can follow this [instructions](https://openjdk.java.net/install/index.html).

2. Gradle `5.4.1`. You can follow this [instructions](https://gradle.org/install/).

## How to use

These commands below will help you to use this project properly.

### When working

#### Running unit test

Open this project directory on terminal and execute this command below:

```console
$ ./gradlew test
```

#### Verifying code style

Open this project directory on terminal and execute this command below:

```console
$ ./gradlew verifyGoogleJavaFormat
```

You can reformat the code by executing:

```console
$ ./gradlew goJF
```

### When building this project

Open this project directory on terminal and execute this command below:

```console
$ ./gradlew clean build
```

You can find the distribution file in
`<this project directory>/build/distributions/todolistapp.zip`.
